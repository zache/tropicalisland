namespace FortuneVoronoi
{
    using Base;

    internal class VDataEvent : VEvent
    {
        public readonly Vector DataPoint;

        public VDataEvent(Vector dp)
        {
            this.DataPoint = dp;
        }

        public override double Y
        {
            get
            {
                return DataPoint.Y;
            }
        }

        public override double X
        {
            get
            {
                return DataPoint.X;
            }
        }

    }
}