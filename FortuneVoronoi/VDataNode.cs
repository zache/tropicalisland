namespace FortuneVoronoi
{
    using Base;

    internal class VDataNode : VNode
    {
        public VDataNode(Vector dp)
        {
            this.DataPoint = dp;
        }

        public readonly Vector DataPoint;
    }
}