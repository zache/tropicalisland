namespace FortuneVoronoi
{
    using System;
    using Base;

    internal abstract class VNode
    {
        private VNode left, right;

        private VNode Left
        {
            get { return left; }
            set
            {
                left = value;
                value.Parent = this;
            }
        }

        private VNode Right
        {
            get { return right; }
            set
            {
                right = value;
                value.Parent = this;
            }
        }

        private VNode Parent { get; set; }

        private void Replace(VNode childOld, VNode childNew)
        {
            if (Left == childOld)
                Left = childNew;
            else if (Right == childOld)
                Right = childNew;
            else throw new Exception("Child not found!");
            childOld.Parent = null;
        }

        public static VDataNode FirstDataNode(VNode root)
        {
            var c = root;
            while (c.Left != null)
                c = c.Left;
            return (VDataNode)c;
        }

        private static VDataNode LeftDataNode(VNode current)
        {
            //1. Up
            do
            {
                if (current.Parent == null)
                    return null;
                if (current.Parent.Left == current)
                {
                    current = current.Parent;
                }
                else
                {
                    current = current.Parent;
                    break;
                }

            } while (true);

            //2. One Left
            current = current.Left;

            //3. Down
            while (current.Right != null)
                current = current.Right;
            return (VDataNode)current; // Cast statt 'as' damit eine Exception kommt
        }

        private static VDataNode RightDataNode(VNode current)
        {
            //1. Up
            do
            {
                if (current.Parent == null)
                    return null;
                if (current.Parent.Right == current)
                {
                    current = current.Parent;
                }
                else
                {
                    current = current.Parent;
                    break;
                }
            } while (true);

            //2. One Right
            current = current.Right;

            //3. Down
            while (current.Left != null)
                current = current.Left;
            return (VDataNode)current; // Cast statt 'as' damit eine Exception kommt
        }

        private static VEdgeNode EdgeToRightDataNode(VNode current)
        {
            //1. Up
            do
            {
                if (current.Parent == null)
                    throw new Exception("No Left Leaf found!");
                if (current.Parent.Right == current)
                {
                    current = current.Parent;
                }
                else
                {
                    current = current.Parent;
                    break;
                }
            } while (true);

            return (VEdgeNode)current;
        }

        private static VDataNode FindDataNode(VNode root, double ys, double x)
        {
            var c = root;
            do
            {
                if (c is VDataNode)
                    return (VDataNode)c;

                c = ((VEdgeNode)c).Cut(ys, x) < 0 ? c.Left : c.Right;
            }
            while (true);
        }

        /// <summary>
        /// Will return the new root (unchanged except in start-up)
        /// </summary>
        public static VNode ProcessDataEvent(VDataEvent e, VNode root, VoronoiGraph graph, double ys, out VDataNode[] circleCheckList)
        {
            if (root == null)
            {
                root = new VDataNode(e.DataPoint);
                circleCheckList = new[] { (VDataNode)root };
                return root;
            }
            //1. Find the node to be replaced
            VNode c = FindDataNode(root, ys, e.DataPoint.X);
            //2. Create the subtree (ONE Edge, but two VEdgeNodes)
            var ve = new VoronoiEdge();
            ve.DelunayB = ((VDataNode)c).DataPoint;
            ve.DelunayA = e.DataPoint;
            ve.VoronoiA = Fortune.Unkown;
            ve.VoronoiB = Fortune.Unkown;
            graph.VoronoiEdges.Add(ve);

            VNode subRoot;
            if (Math.Abs(ve.DelunayB.Y - ve.DelunayA.Y) < 1e-10)
            {
                if (ve.DelunayB.X < ve.DelunayA.X)
                {
                    subRoot = new VEdgeNode(ve, false)
                                  {
                                      Left = new VDataNode(ve.DelunayB),
                                      Right = new VDataNode(ve.DelunayA)
                                  };
                }
                else
                {
                    subRoot = new VEdgeNode(ve, true)
                                  {
                                      Left = new VDataNode(ve.DelunayA),
                                      Right = new VDataNode(ve.DelunayB)
                                  };
                }
                circleCheckList = new[] { (VDataNode)subRoot.Left, (VDataNode)subRoot.Right };
            }
            else
            {
                subRoot = new VEdgeNode(ve, false)
                              {
                                  Left = new VDataNode(ve.DelunayB),
                                  Right = new VEdgeNode(ve, true)
                                          {
                                              Left = new VDataNode(ve.DelunayA),
                                              Right = new VDataNode(ve.DelunayB)
                                          }
                              };
                circleCheckList = new[] { (VDataNode)subRoot.Left, (VDataNode)subRoot.Right.Left, (VDataNode)subRoot.Right.Right };
            }

            //3. Apply subtree
            if (c.Parent == null)
                return subRoot;

            c.Parent.Replace(c, subRoot);
            return root;
        }
        public static VNode ProcessCircleEvent(VCircleEvent e, VNode Root, VoronoiGraph VG, double ys, out VDataNode[] CircleCheckList)
        {
            VDataNode a, b, c;
            VEdgeNode eu, eo;
            b = e.NodeN;
            a = LeftDataNode(b);
            c = RightDataNode(b);
            if (a == null || b.Parent == null || c == null || !a.DataPoint.Equals(e.NodeL.DataPoint) || !c.DataPoint.Equals(e.NodeR.DataPoint))
            {
                CircleCheckList = new VDataNode[] { };
                return Root; // Abbruch da sich der Graph ver�ndert hat
            }
            eu = (VEdgeNode)b.Parent;
            CircleCheckList = new VDataNode[] { a, c };

            //1. Create the new Vertex
            Vector VNew = new Vector(e.Center.X, e.Center.Y);
            VG.Vertizes.Add(VNew);

            //2. Find out if a or c are in a distand part of the tree (the other is then b's sibling) and assign the new vertex
            if (eu.Left == b) // c is sibling
            {
                eo = VNode.EdgeToRightDataNode(a);

                // replace eu by eu's Right
                eu.Parent.Replace(eu, eu.Right);
            }
            else // a is sibling
            {
                eo = VNode.EdgeToRightDataNode(b);

                // replace eu by eu's Left
                eu.Parent.Replace(eu, eu.Left);
            }
            eu.Edge.AddVertex(VNew);

            eo.Edge.AddVertex(VNew);


            //2. Replace eo by new Edge
            VoronoiEdge VE = new VoronoiEdge();
            VE.DelunayB = a.DataPoint;
            VE.DelunayA = c.DataPoint;
            VE.AddVertex(VNew);
            VG.VoronoiEdges.Add(VE);

            VEdgeNode VEN = new VEdgeNode(VE, false);
            VEN.Left = eo.Left;
            VEN.Right = eo.Right;
            if (eo.Parent == null)
                return VEN;
            eo.Parent.Replace(eo, VEN);
            return Root;
        }
        public static VCircleEvent CircleCheckDataNode(VDataNode n, double ys)
        {
            var l = LeftDataNode(n);
            var r = RightDataNode(n);

            if (l == null || r == null || l.DataPoint == r.DataPoint || l.DataPoint == n.DataPoint || n.DataPoint == r.DataPoint)
                return null;

            if (MathTools.CounterClockwise(l.DataPoint.X, l.DataPoint.Y, n.DataPoint.X, n.DataPoint.Y, r.DataPoint.X, r.DataPoint.Y, false) <= 0)
                return null;

            var center = Fortune.CircumCircleCenter(l.DataPoint, n.DataPoint, r.DataPoint);
            var vc = new VCircleEvent
                         {
                             NodeN = n, 
                             NodeL = l, 
                             NodeR = r, 
                             Center = center, 
                             Valid = true
                         };

            if (vc.Y > ys || Math.Abs(vc.Y - ys) < 1e-10)
                return vc;

            return null;
        }

        public static void CleanUpTree(VNode root)
        {
            var ve = root as VEdgeNode;
            if (ve == null)
                return;

            while (ve.Edge.VoronoiB == Fortune.Unkown)
            {
                ve.Edge.AddVertex(Fortune.Infinite);
            }

            if (ve.Flipped)
            {
                var T = ve.Edge.DelunayB;
                ve.Edge.DelunayB = ve.Edge.DelunayA;
                ve.Edge.DelunayA = T;
            }

            ve.Edge.Done = true;

            CleanUpTree(root.Left);
            CleanUpTree(root.Right);
        }
    }
}