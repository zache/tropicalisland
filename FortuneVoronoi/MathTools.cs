namespace FortuneVoronoi
{
    using System;

    public abstract class MathTools
    {
        public static double Distance(double x1, double y1, double x2, double y2)
        {
            return Math.Sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        }

        public static int CounterClockwise(double p0X, double p0Y, double p1X, double p1Y, double p2X, double p2Y, bool plusOneOnZeroDegrees)
        {
            var dx1 = p1X - p0X; var dy1 = p1Y - p0Y;
            var dx2 = p2X - p0X; var dy2 = p2Y - p0Y;
            if (dx1 * dy2 > dy1 * dx2) return +1;
            if (dx1 * dy2 < dy1 * dx2) return -1;
            if ((dx1 * dx2 < 0) || (dy1 * dy2 < 0)) return -1;
            if ((dx1 * dx1 + dy1 * dy1) < (dx2 * dx2 + dy2 * dy2) && plusOneOnZeroDegrees)
                return +1;
            return 0;
        }
    }
}