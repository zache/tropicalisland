namespace FortuneVoronoi
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using Base;
    using BenTools.Data;

    public abstract class Fortune
    {
        public static readonly Vector Infinite = new Vector(double.PositiveInfinity, double.PositiveInfinity);
        public static readonly Vector Unkown = new Vector(double.NaN, double.NaN);

        internal static double ParabolicCut(double x1, double y1, double x2, double y2, double ys)
        {
            if (Math.Abs(x1 - x2) < 1e-10 && Math.Abs(y1 - y2) < 1e-10)
                throw new Exception("Identical datapoints are not allowed!");

            if (Math.Abs(y1 - ys) < 1e-10 && Math.Abs(y2 - ys) < 1e-10)
                return (x1 + x2) / 2;
            if (Math.Abs(y1 - ys) < 1e-10)
                return x1;
            if (Math.Abs(y2 - ys) < 1e-10)
                return x2;
            double a1 = 1 / (2 * (y1 - ys));
            double a2 = 1 / (2 * (y2 - ys));
            if (Math.Abs(a1 - a2) < 1e-10)
                return (x1 + x2) / 2;
            double xs1 = 0.5 / (2 * a1 - 2 * a2) * (4 * a1 * x1 - 4 * a2 * x2 + 2 * Math.Sqrt(-8 * a1 * x1 * a2 * x2 - 2 * a1 * y1 + 2 * a1 * y2 + 4 * a1 * a2 * x2 * x2 + 2 * a2 * y1 + 4 * a2 * a1 * x1 * x1 - 2 * a2 * y2));
            double xs2 = 0.5 / (2 * a1 - 2 * a2) * (4 * a1 * x1 - 4 * a2 * x2 - 2 * Math.Sqrt(-8 * a1 * x1 * a2 * x2 - 2 * a1 * y1 + 2 * a1 * y2 + 4 * a1 * a2 * x2 * x2 + 2 * a2 * y1 + 4 * a2 * a1 * x1 * x1 - 2 * a2 * y2));
            xs1 = Math.Round(xs1, 10);
            xs2 = Math.Round(xs2, 10);
            if (xs1 > xs2)
            {
                double h = xs1;
                xs1 = xs2;
                xs2 = h;
            }
            if (y1 >= y2)
                return xs2;
            return xs1;
        }

        internal static Vector CircumCircleCenter(Vector a, Vector b, Vector c)
        {
            if (Equals(a, b) || Equals(b, c) || Equals(a, c))
                throw new Exception("Need three different points!");

            var tx = (a.X + c.X) / 2;
            var ty = (a.Y + c.Y) / 2;

            var vx = (b.X + c.X) / 2;
            var vy = (b.Y + c.Y) / 2;

            double ux, uy, wx, wy;

            if (Math.Abs(a.X - c.X) < 1e-10)
            {
                ux = 1;
                uy = 0;
            }
            else
            {
                ux = (c.Y - a.Y) / (a.X - c.X);
                uy = 1;
            }

            if (Math.Abs(b.X - c.X) < 1e-10)
            {
                wx = -1;
                wy = 0;
            }
            else
            {
                wx = (b.Y - c.Y) / (b.X - c.X);
                wy = -1;
            }

            var alpha = (wy * (vx - tx) - wx * (vy - ty)) / (ux * wy - wx * uy);

            return new Vector(tx + alpha * ux, ty + alpha * uy);
        }

        public static VoronoiGraph ComputeVoronoiGraph(IEnumerable<Vector> datapoints)
        {
            var pq = new BinaryPriorityQueue();
            var currentCircles = new Hashtable();
            var vg = new VoronoiGraph();
            
            VNode rootNode = null;

            foreach (var v in datapoints)
            {
                pq.Push(new VDataEvent(v));
            }

            while (pq.Count > 0)
            {
                var ve = pq.Pop() as VEvent;
                VDataNode[] circleCheckList;

                if (ve is VDataEvent)
                {
                    rootNode = VNode.ProcessDataEvent(ve as VDataEvent, rootNode, vg, ve.Y, out circleCheckList);
                }
                else if (ve is VCircleEvent)
                {
                    currentCircles.Remove(((VCircleEvent)ve).NodeN);
                    if (!((VCircleEvent)ve).Valid)
                        continue;
                    rootNode = VNode.ProcessCircleEvent(ve as VCircleEvent, rootNode, vg, ve.Y, out circleCheckList);
                }
                else
                {
                    throw new Exception("Got event of type " + ve.GetType().ToString() + "!");
                }

                foreach (var vd in circleCheckList)
                {
                    if (currentCircles.ContainsKey(vd))
                    {
                        ((VCircleEvent)currentCircles[vd]).Valid = false;
                        currentCircles.Remove(vd);
                    }
                    var vce = VNode.CircleCheckDataNode(vd, ve.Y);

                    if (vce != null)
                    {
                        pq.Push(vce);
                        currentCircles[vd] = vce;
                    }
                }

                if (ve is VDataEvent)
                {
                    var dp = ((VDataEvent)ve).DataPoint;

                    foreach (VCircleEvent vce in currentCircles.Values)
                    {
                        var one = MathTools.Distance(dp.X, dp.Y, vce.Center.X, vce.Center.Y);
                        var two = Math.Abs(MathTools.Distance(dp.X, dp.Y, vce.Center.X, vce.Center.Y) - (vce.Y - vce.Center.Y));

                        if (one < vce.Y - vce.Center.Y && two > 1e-10)
                            vce.Valid = false;
                    }
                }
            }

            VNode.CleanUpTree(rootNode);

            foreach (var ve in vg.VoronoiEdges)
            {
                if (ve.Done)
                    continue;

                if (ve.VoronoiB == Unkown)
                {
                    ve.AddVertex(Infinite);
                    if (Math.Abs(ve.DelunayB.Y - ve.DelunayA.Y) < 1e-10 && ve.DelunayB.X < ve.DelunayA.X)
                    {
                        var temp = ve.DelunayB;
                        ve.DelunayB = ve.DelunayA;
                        ve.DelunayA = temp;
                    }
                }
            }

            var minuteEdges = new ArrayList();
            foreach (var ve in vg.VoronoiEdges)
            {
                if (!ve.IsPartlyInfinite && ve.VoronoiA.Equals(ve.VoronoiB))
                {
                    minuteEdges.Add(ve);
                    // prevent rounding errors from expanding to holes
                    foreach (var ve2 in vg.VoronoiEdges)
                    {
                        if (ve2.VoronoiA.Equals(ve.VoronoiA))
                            ve2.VoronoiA = ve.VoronoiA;
                        if (ve2.VoronoiB.Equals(ve.VoronoiA))
                            ve2.VoronoiB = ve.VoronoiA;
                    }
                }
            }

            foreach (VoronoiEdge ve in minuteEdges)
                vg.VoronoiEdges.Remove(ve);

            return vg;
        }
    }
}
