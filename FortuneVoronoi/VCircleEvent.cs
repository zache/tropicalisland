namespace FortuneVoronoi
{
    using System;
    using System.Windows;
    using Base;

    internal class VCircleEvent : VEvent
    {
        public VDataNode NodeN, NodeL, NodeR;
        public Vector Center;

        public override double Y
        {
            get
            {
                return Math.Round(Center.Y + MathTools.Distance(NodeN.DataPoint.X, NodeN.DataPoint.Y, Center.X, Center.Y), 10);
            }
        }

        public override double X
        {
            get
            {
                return Center.X;
            }
        }

        public bool Valid = true;
    }
}