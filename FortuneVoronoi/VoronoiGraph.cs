namespace FortuneVoronoi
{
    using System.Collections.Generic;
    using Base;

    public class VoronoiGraph
    {
        public readonly HashSet<Vector> Vertizes = new HashSet<Vector>();
        public readonly HashSet<VoronoiEdge> VoronoiEdges = new HashSet<VoronoiEdge>();
    }
}