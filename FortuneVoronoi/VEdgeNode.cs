namespace FortuneVoronoi
{
    using System;

    internal class VEdgeNode : VNode
    {
        public VEdgeNode(VoronoiEdge E, bool Flipped)
        {
            this.Edge = E;
            this.Flipped = Flipped;
        }
        public VoronoiEdge Edge;
        public bool Flipped;
        public double Cut(double ys, double x)
        {
            if(!Flipped)
                return Math.Round(x-Fortune.ParabolicCut(Edge.DelunayB.X, Edge.DelunayB.Y, Edge.DelunayA.X, Edge.DelunayA.Y, ys),10);
            return Math.Round(x-Fortune.ParabolicCut(Edge.DelunayA.X, Edge.DelunayA.Y, Edge.DelunayB.X, Edge.DelunayB.Y, ys),10);
        }
    }
}