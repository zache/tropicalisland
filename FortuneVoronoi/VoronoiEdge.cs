namespace FortuneVoronoi
{
    using System;
    using Base;

    public class VoronoiEdge
    {
        internal bool Done;

        public Vector DelunayA, DelunayB;
        public Vector VoronoiA = Fortune.Unkown, VoronoiB = Fortune.Unkown;

        public void AddVertex(Vector v)
        {
            if (VoronoiA == Fortune.Unkown)
                VoronoiA = v;
            else if (VoronoiB == Fortune.Unkown)
                VoronoiB = v;
            else throw new Exception("Tried to add third vertex!");
        }

        public bool IsPartlyInfinite
        {
            get { return VoronoiA.Equals(Fortune.Infinite) || VoronoiB.Equals(Fortune.Infinite); }
        }
    }
}