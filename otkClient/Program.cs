﻿namespace otkClient
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using Base;
    using OpenTK;
    using OpenTK.Graphics.OpenGL;
    using OpenTK.Input;
    using Voronoi;

    class Program
    {
        const double Left = -500.0;
        const double Right = 500.0;
        const double Top = 500.0;
        const double Bottom = -500.0;

        private static VoronoiMap graph;

        private static VoronoiMap BuildGraph()
        {
            var vectors = new List<Vector>();
            var r = new Random();

            for (var i = 0; i < 600; i++)
            {
                var x = -500.0 + (1000.0 * r.NextDouble());
                var y = -500.0 + (1000.0 * r.NextDouble());
                vectors.Add(new Vector(x, y));
            }

            var builder = new MapBuilder();
            vectors = builder.Smooth(vectors, Left, Right, Top, Bottom).ToList();

            return builder.BuildGraph(vectors, Left, Right, Top, Bottom);
        }

        [STAThread]
        public static void Main()
        {
            using (var game = new GameWindow())
            {

                graph = BuildGraph();

                game.Load += (sender, e) =>
                {
                    // setup settings, load textures, sounds
                    game.VSync = VSyncMode.On;
                };

                game.Resize += (sender, e) =>
                {
                    GL.Viewport(0, 0, game.Width, game.Height);
                };

                game.UpdateFrame += (sender, e) =>
                {
                    // add game logic, input handling
                    if (game.Keyboard[Key.Escape])
                    {
                        game.Exit();
                    }
                    else if (game.Keyboard[Key.Space])
                    {
                        graph = BuildGraph();
                    }
                };

                game.RenderFrame += (sender, e) =>
                {
                    // render graphics
                    GL.Clear(ClearBufferMask.ColorBufferBit | ClearBufferMask.DepthBufferBit);

                    GL.MatrixMode(MatrixMode.Projection);
                    GL.LoadIdentity();
                    GL.Ortho(Left - 100, Right + 100, Bottom - 100, Top + 100, 0.0, 30.0);

                    GL.LineWidth(1.0f);

                    foreach (var center in graph.Centers.Values)
                    {
                        var points = center.Corners.Where(c => !Vector.IsInfinite(c.Location)).Select(c => c.Location).ToArray();
                       
                        if (center.IsWater)
                        {
                            GL.Color3(center.Ocean ? Color.DodgerBlue : Color.CornflowerBlue);
                            GL.Begin(PrimitiveType.Polygon);
                            foreach (var p in points)
                                GL.Vertex2(p.X, p.Y);

                            GL.End();
                        }
                        else
                        {
                            GL.Color3(Color.Green);
                            GL.Begin(PrimitiveType.Polygon);
                            foreach (var p in points)
                                GL.Vertex2(p.X, p.Y);
                            GL.End();
                        }

                        GL.Begin(PrimitiveType.LineLoop);
                        GL.Color3(Color.Black);
                        
                        foreach (var p in points)
                            GL.Vertex2(p.X, p.Y);

                        GL.End();
                    }

                    game.SwapBuffers();
                };

                // Run the game at 60 updates per second
                game.Run();
            }
        }
    }
}
