﻿namespace Voronoi
{
    using System;
    using System.Collections.Generic;
    using Base;

    public class Corner : IEquatable<Corner>
    {
        public Corner()
        {
            Touches = new HashSet<Center>();
            Protrudes = new HashSet<MapEdge>();
            Adjacent = new HashSet<Corner>();
        }

        public Vector Location { get; set; }

        public HashSet<Center> Touches { get; private set; }
        public HashSet<MapEdge> Protrudes { get; private set; }
        public HashSet<Corner> Adjacent { get; private set; }
       
        public bool IsWater { get; set; }
        public bool Ocean { get; set; }

        // To costal corner (DownSlope if it is Coastal)
        // public Corner Watershed { get; set; }
        // Adjacent corner with lowest elevation
        // public Corner DownSlope { get; set; }

        // bool: Ocean, Water (Lake or Ocean), Coast, Border
        // double: Elevation, Moisture
        // int: River, WatershedSize

        public override bool Equals(object obj)
        {
            if ((obj == null) || !(obj is Corner))
                return false;

            return this.Equals((Corner)obj);
        }

        public override int GetHashCode()
        {
            return Location.GetHashCode();
        }

        public bool Equals(Corner other)
        {
            if (other == null)
                return false;

            return this.Location == other.Location;
        }
    }
}