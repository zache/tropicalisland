﻿namespace Voronoi
{
    using System;
    using System.Collections.Generic;
    using Base;

    public class Center : IEquatable<Center>
    {
        private bool ocean;

        public Center()
        {
            Neighbours = new HashSet<Center>();
            Borders = new HashSet<MapEdge>();
            Corners = new HashSet<Corner>();
        }

        public Vector Location { get; set; }

        public HashSet<Center> Neighbours { get; private set; }
        public HashSet<MapEdge> Borders { get; private set; }
        public HashSet<Corner> Corners { get; set; }

        public bool IsWater { get; set; }

        public bool Ocean
        {
            get { return ocean; }
            set
            {
                ocean = value;
                if (Ocean)
                    IsWater = true;
            }
        }

        public bool Coast { get; set; }

        // bool: Water (Lake or Ocean), Ocean, Coast, Border
        // string: Biome
        // dobule: Elevation, Moisture

        public override bool Equals(object obj)
        {
            if ((obj == null) || !(obj is Center))
                return false;

            return this.Equals((Center)obj);
        }

        public override int GetHashCode()
        {
            return Location.GetHashCode();
        }

        public bool Equals(Center other)
        {
            if (other == null)
                return false;

            return this.Location == other.Location;
        }
    }
}