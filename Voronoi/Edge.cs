namespace Voronoi
{
    using System;
    using Base;

    public struct Edge : IEquatable<Edge>
    {
        public readonly Vector A;
        public readonly Vector B;

        public Edge(double aX, double aY, double bX, double bY)
        {
            A = new Vector(aX, aY);
            B = new Vector(bX, bY);
        }

        public Edge(Vector a, Vector b)
            : this()
        {
            A = a;
            B = b;
        }

        public bool Equals(Edge other)
        {
            return (this.A == other.A && this.B == other.B) || (this.B == other.A && this.A == other.B);
        }

        public static bool operator ==(Edge e1, Edge e2)
        {
            return e1.Equals(e2);
        }

        public static bool operator !=(Edge e1, Edge e2)
        {
            return !(e1 == e2);
        }

        public override bool Equals(object obj)
        {
            if ((obj == null) || !(obj is Edge))
                return false;

            return this.Equals((Edge)obj);
        }

        public override int GetHashCode()
        {
            return A.GetHashCode() ^ B.GetHashCode();
        }
    }
}