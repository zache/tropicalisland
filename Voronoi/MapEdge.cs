﻿namespace Voronoi
{
    using System;
    using Base;

    public class MapEdge : IEquatable<MapEdge>
    {
        public Vector MidPoint { get { return Vector.Middle(VoronoiEdge.A, VoronoiEdge.B); } }

        public Edge DelunayEdge { get; set; }
        public Edge VoronoiEdge { get; set; }

        // int river (volume of water, 0 not a river)

        public override bool Equals(object obj)
        {
            if ((obj == null) || !(obj is MapEdge))
                return false;

            return this.Equals((MapEdge)obj);
        }

        public override int GetHashCode()
        {
            return DelunayEdge.GetHashCode() ^ VoronoiEdge.GetHashCode();
        }

        public bool Equals(MapEdge other)
        {
            if (other == null)
                return false;

            return this.DelunayEdge == other.DelunayEdge && this.VoronoiEdge == other.VoronoiEdge;
        }
    }
}
