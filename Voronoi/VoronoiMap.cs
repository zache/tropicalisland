﻿namespace Voronoi
{
    using System.Collections.Generic;
    using Base;

    public class VoronoiMap
    {
        public HashSet<MapEdge> Edges { get; set; }
        public Dictionary<Vector, Center> Centers { get; set; }
        public Dictionary<Vector, Corner> Corners { get; set; }
    }
}
