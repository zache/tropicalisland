﻿namespace Voronoi
{
    using System;
    using System.Collections.Generic;
    using System.Diagnostics;
    using System.Linq;
    using Base;
    using FortuneVoronoi;

    public class MapBuilder
    {
        // Procedure:
        // Select points
        // Get Voronoi
        // Relax Voronoi
        // Build graph (Center, Edges, Corners)
        //      Improve corners
        // Assign elevation
        //      Assign corner elevation
        //      Assign ocean, land and coast
        //      Redistribute elevation
        //      (Bottom out ocean)
        //      Assign polygon elevation (average of its corners)
        // Assign moisture
        //      Calculate downslopes
        //      Calculate watersheds
        //      Create rivers
        //      Assign corner moisture
        //      Redistribute moisture
        //      Assign polygon moisture (average of its corners)
        // Decorate with biome

        public VoronoiMap BuildGraph(IEnumerable<Vector> points, double left, double right, double top, double bottom)
        {
            var graph = Fortune.ComputeVoronoiGraph(points.Distinct(VectorComparer));

            var mapEdges = new HashSet<MapEdge>();
            var centers = new Dictionary<Vector, Center>();
            var corners = new Dictionary<Vector, Corner>();

            foreach (var v in graph.VoronoiEdges)
            {
                var ocean = false;

                if (Vector.IsInfinite(v.VoronoiB))
                {
                    continue;
                }

                if (v.VoronoiA.Y < bottom)
                {
                    ocean = true;
                    //   v.VoronoiA = new Vector(v.VoronoiA.X, 0.1);
                }
                if (v.VoronoiA.X < left)
                {
                    ocean = true;
                    //  v.VoronoiA = new Vector(0.1, v.VoronoiA.Y);
                }
                if (v.VoronoiB.X < left)
                {
                    ocean = true;
                    //  v.VoronoiB = new Vector(0.1, v.VoronoiB.Y);
                }
                if (v.VoronoiB.Y < bottom)
                {
                    ocean = true;
                    //  v.VoronoiB = new Vector(v.VoronoiB.X, 0.1);
                }

                if (v.VoronoiA.Y > top)
                {
                    ocean = true;
                    //  v.VoronoiA = new Vector(v.VoronoiA.X, height);
                }
                if (v.VoronoiA.X > right)
                {
                    ocean = true;
                    //  v.VoronoiA = new Vector(width, v.VoronoiA.Y);
                }
                if (v.VoronoiB.X > right)
                {
                    ocean = true;
                    //  v.VoronoiB = new Vector(width, v.VoronoiB.Y);
                }
                if (v.VoronoiB.Y > top)
                {
                    ocean = true;
                    // v.VoronoiB = new Vector(v.VoronoiB.X, height);
                }

                var mapEdge = new MapEdge
                {
                    DelunayEdge = new Edge(v.DelunayA, v.DelunayB),
                    VoronoiEdge = new Edge(v.VoronoiA, v.VoronoiB)
                };

                mapEdges.Add(mapEdge);

                if (!centers.ContainsKey(v.DelunayA))
                    centers.Add(v.DelunayA,
                        new Center
                        {
                            Location = v.DelunayA,
                            IsWater = IsWater(v.DelunayA)
                        });

                if (!centers.ContainsKey(v.DelunayB))
                    centers.Add(v.DelunayB,
                        new Center
                        {
                            Location = v.DelunayB,
                            IsWater = IsWater(v.DelunayB)
                        });

                if (!corners.ContainsKey(v.VoronoiA))
                    corners.Add(v.VoronoiA,
                        new Corner
                        {
                            Location = v.VoronoiA
                        });

                if (!corners.ContainsKey(v.VoronoiB))
                    corners.Add(v.VoronoiB,
                        new Corner
                        {
                            Location = v.VoronoiB
                        });

                var centerOne = centers[v.DelunayA];
                var centerTwo = centers[v.DelunayB];

                centerOne.Ocean = ocean;
                centerTwo.Ocean = ocean;

                var cornerOne = corners[v.VoronoiA];
                var cornerTwo = corners[v.VoronoiB];

                // Center neighbours centers
                centerOne.Neighbours.Add(centerTwo);
                centerTwo.Neighbours.Add(centerOne);

                // Center borders MapEdge
                centerOne.Borders.Add(mapEdge);
                centerTwo.Borders.Add(mapEdge);

                // Center has corners
                centerOne.Corners.Add(cornerOne);
                centerOne.Corners.Add(cornerTwo);
                centerTwo.Corners.Add(cornerOne);
                centerTwo.Corners.Add(cornerTwo);

                // Corner touches centers
                cornerOne.Touches.Add(centerOne);
                cornerOne.Touches.Add(centerTwo);
                cornerOne.Touches.Add(centerOne);
                cornerTwo.Touches.Add(centerTwo);

                // MapEdge protrudes from corner
                cornerOne.Protrudes.Add(mapEdge);
                cornerTwo.Protrudes.Add(mapEdge);

                // Corner is adjacent to corners
                cornerOne.Adjacent.Add(cornerTwo);
                cornerTwo.Adjacent.Add(cornerOne);
            }

            foreach (var center in centers.Values)
            {
                center.Corners = Sort(center.Corners.Distinct(CornerComparer)).ToHashSet();

                foreach (var c in center.Corners)
                {
                    if (center.IsWater)
                        c.IsWater = true;

                    if (center.Ocean)
                        c.Ocean = true;
                }
            }

            foreach (var center in centers.Values.Where(c => !c.IsWater))
            {
                var waterCount = center.Corners.Count(c => c.IsWater);

                if (waterCount > 0)
                {
                    if (waterCount > 2)
                    {
                        center.IsWater = true;

                        if (center.Corners.Any(c => c.Ocean))
                            center.Ocean = true;
                    }
                    else
                    {
                        center.Coast = true;
                    }
                }
            }

            var queue = new Queue<Center>(centers.Values.Where(c => c.Ocean));

            while (queue.Count > 0)
            {
                var center = queue.Dequeue();
                center.Ocean = true;

                foreach (var c in center.Corners)
                {
                    c.IsWater = true;
                    c.Ocean = true;

                    foreach (var cent in c.Touches.Where(ce => !ce.Ocean).Where(ce => ce.IsWater))
                    {
                        if (cent.IsWater && !cent.Ocean)
                            queue.Enqueue(cent);
                        else
                        {
                            cent.Coast = true;
                        }
                    }
                }
            }

            return new VoronoiMap { Centers = centers, Corners = corners, Edges = mapEdges };
        }

        public IEnumerable<Vector> Smooth(IEnumerable<Vector> vectors, double left, double right, double top, double bottom)
        {
            var previous = vectors.ToList();
            var next = new List<Vector>();

            for (var i = 0; i < 3; i++)
            {
                var map = Fortune.ComputeVoronoiGraph(previous);

                foreach (var vector in previous)
                {
                    var v0 = 0.0d;
                    var v1 = 0.0d;
                    var say = 0;

                    foreach (var edge in map.VoronoiEdges)
                    {
                        if (edge.DelunayB == vector || edge.DelunayA == vector)
                        {
                            var p0 = (edge.VoronoiA.X + edge.VoronoiB.X) / 2;
                            var p1 = (edge.VoronoiA.Y + edge.VoronoiB.Y) / 2;
                            v0 += double.IsNaN(p0) ? 0 : p0;
                            v1 += double.IsNaN(p1) ? 0 : p1;
                            say++;
                        }
                    }

                    var newX = vector.X;
                    var newY = vector.Y;

                    if (((v0 / say) < left) && ((v0 / say) > right))
                    {
                        newX = v0 / say;
                    }

                    if (((v1 / say) < top) && ((v1 / say) > bottom))
                    {
                        newY = v1 / say;
                    }

                    next.Add(new Vector(newX, newY));
                }

                previous = next;
                next = new List<Vector>();
            }

            return previous;
        }

        private static readonly SloppyVectorComparer VectorComparer = new SloppyVectorComparer();
        private static readonly SloppyCornerComparer CornerComparer = new SloppyCornerComparer(VectorComparer);

        private IEnumerable<Corner> Sort(IEnumerable<Corner> corners)
        {
            var vectors = corners as IList<Corner> ?? corners.ToList();

            var centriod = vectors.Aggregate(Vector.Zero, (current, v) => current + v.Location);
            centriod /= vectors.Count;

            Func<Vector, Corner, double> heading = (c, v) =>
                {
                    var direction = v.Location - c;
                    return Math.Atan2(direction.Y, direction.X);
                };

            return vectors.OrderBy(v => heading(centriod, v));
        }

        private IEnumerable<Vector> Sort(IEnumerable<Vector> enumerable)
        {
            var vectors = enumerable as IList<Vector> ?? enumerable.ToList();

            var centriod = vectors.Aggregate(Vector.Zero, (current, v) => current + v);
            centriod /= vectors.Count;

            Func<Vector, Vector, double> heading = (c, v) =>
                {
                    var direction = v - c;
                    return Math.Atan2(direction.Y, direction.X);
                };

            return vectors.OrderBy(v => heading(centriod, v));
        }

        private class SloppyCornerComparer : IEqualityComparer<Corner>
        {
            private readonly IEqualityComparer<Vector> vectorComparer;

            public SloppyCornerComparer(IEqualityComparer<Vector> vectorComparer)
            {
                this.vectorComparer = vectorComparer;
            }

            public bool Equals(Corner x, Corner y)
            {
                return vectorComparer.Equals(x.Location, y.Location);
            }

            public int GetHashCode(Corner obj)
            {
                return vectorComparer.GetHashCode(obj.Location);
            }
        }

        private class SloppyVectorComparer : IEqualityComparer<Vector>
        {
            public bool Equals(Vector a, Vector b)
            {
                return Math.Abs(a.X - b.X) < 20 && Math.Abs(a.Y - b.Y) < 20;
            }

            public int GetHashCode(Vector obj)
            {
                return obj.GetHashCode();
            }
        }

        private bool IsWater(Vector v)
        {
            var x = PerlinNoise.Noise(v.X, v.Y, 0);

            return x * x > 0.2;
        }
    }
}
