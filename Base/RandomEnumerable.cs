﻿namespace Base
{
    using System;
    using System.Collections;
    using System.Collections.Generic;

    public class RandomEnumerable<T> : IEnumerable<T>
    {
        private readonly RandomEnumerator enumerator;

        public RandomEnumerable(IEnumerable<T> innerEnumerable)
        {
            enumerator = new RandomEnumerator(innerEnumerable.GetEnumerator());
        }

        private class RandomEnumerator : IEnumerator<T>
        {
            private readonly IEnumerator<T> innerEnumerator;
            private readonly Random random = new Random();

            private bool skipping;
            private int count;

            public RandomEnumerator(IEnumerator<T> innerEnumerator)
            {
                this.innerEnumerator = innerEnumerator;
            }

            public void Dispose()
            {
                innerEnumerator.Dispose();
            }

            public bool MoveNext()
            {
                if (count > 0)
                {
                    if (skipping)
                    {
                        for (var i = 0; i <= count; i++)
                            if (innerEnumerator.MoveNext())
                                count--;
                            else
                                return false;
                        
                        return innerEnumerator.MoveNext();
                    }
                    else
                    {
                        count--;
                        return innerEnumerator.MoveNext();
                    }
                }
                else
                {
                    count = random.Next(5, 20);
                    skipping = !skipping;
                    return MoveNext();
                }
            }

            public void Reset()
            {
                innerEnumerator.Reset();
            }

            public T Current { get { return innerEnumerator.Current; } }

            object IEnumerator.Current
            {
                get { return Current; }
            }
        }

        public IEnumerator<T> GetEnumerator()
        {
            return enumerator;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}