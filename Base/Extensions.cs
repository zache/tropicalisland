﻿namespace Base
{
    using System;
    using System.Collections.Generic;

    public static class Extensions
    {
        public static IEnumerable<T> AsRandom<T>(this IEnumerable<T> enumerable)
        {
            return new RandomEnumerable<T>(enumerable);
        }

        public static IEnumerable<T> Repeat<T>(this IEnumerable<T> enumerable, int count)
        {
            if (count <= 0)
                throw new ArgumentOutOfRangeException("count", "Count less than zero");

            var currentCount = 0;

            var enumerator = enumerable.GetEnumerator();

            while (currentCount < count)
            {
                while (enumerator.MoveNext())
                {
                    yield return enumerator.Current;
                }

                enumerator.Reset();
                currentCount++;
            }
        }


        public static HashSet<T> ToHashSet<T>(this IEnumerable<T> enumerable)
        {
            return new HashSet<T>(enumerable);
        }
    }
}