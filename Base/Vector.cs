﻿namespace Base
{
    using System;

    public struct Vector : IEquatable<Vector>
    {
        public readonly double X;
        public readonly double Y;

        public Vector(double x, double y)
            : this()
        {
            X = x;
            Y = y;
        }

        public static readonly Vector Zero = new Vector(0, 0);

        public double Length { get { return Math.Sqrt(X * X + Y * Y); } }

        public double LengthSquared { get { return X * X + Y * Y; } }

        public static bool IsPartlyInfinite(Vector v)
        {
            return double.IsInfinity(v.X) || double.IsInfinity(v.Y);
        }

        public static bool IsUnknown(Vector v)
        {
            return double.IsNaN(v.X) || double.IsNaN(v.Y);
        }

        public static bool IsInfinite(Vector v)
        {
            return double.IsInfinity(v.X) && double.IsInfinity(v.Y);
        }

        public Vector ToNormal()
        {
            var m = Math.Abs(X);
            var absy = Math.Abs(Y);

            if (absy > m)
            {
                m = absy;
            }

            var x = X / m;
            var y = Y / m;

            var length = Math.Sqrt(x * x + y * y);

            return new Vector(x / length, y / length);
        }

        public static Vector Middle(Vector a, Vector b)
        {
            return new Vector((a.X + b.X) / 2, (a.Y + b.Y) / 2);
        }

        public static double Distance(Vector a, Vector b)
        {
            return Math.Sqrt((b.X - a.X) * (b.X - a.X) + (b.Y - a.Y) * (b.Y - a.Y));
        }

        public bool Equals(Vector other)
        {
            if (double.IsNaN(X) && double.IsNaN(Y) && double.IsNaN(other.X) && double.IsNaN(other.Y))
                return true;

            if (double.IsInfinity(X) && double.IsInfinity(Y) && double.IsInfinity(other.X) && double.IsInfinity(other.Y))
                return true;

            return Math.Abs(this.X - other.X) < 1e-10 && Math.Abs(this.Y - other.Y) < 1e-10;
        }

        public static bool operator ==(Vector a, Vector b)
        {
            return a.Equals(b);
        }

        public static bool operator !=(Vector p1, Vector p2)
        {
            return !(p1 == p2);
        }

        public static Vector operator +(Vector a, Vector b)
        {
            return new Vector(a.X + b.X, a.Y + b.Y);
        }

        public static Vector operator -(Vector a, Vector b)
        {
            return new Vector(a.X - b.X, a.Y - b.Y);
        }

        public static Vector operator *(Vector vector, double scalar)
        {
            return new Vector(vector.X * scalar,
                              vector.Y * scalar);
        }

        public static Vector operator /(Vector vector, double scalar)
        {
            return vector * (1.0 / scalar);
        }

        public override bool Equals(object obj)
        {
            if ((obj == null) || !(obj is Vector))
                return false;

            return this.Equals((Vector)obj);
        }

        public override int GetHashCode()
        {
            return X.GetHashCode() ^ Y.GetHashCode();
        }

        public override string ToString()
        {
            return "(" + X + "," + Y + ")";
        }
    }
}
