﻿namespace VoroMap
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Drawing;
    using Base;
    using FortuneVoronoi;
    using Voronoi;

    public static class Maps
    {
        public static Bitmap Draw(int width, int height, VoronoiMap map)
        {
            var bmp = new Bitmap(width, height);
            var g = Graphics.FromImage(bmp);
            
            foreach (var center in map.Centers.Values)
            {
                var points = center.Corners.Where(c => !Vector.IsInfinite(c.Location)).Select(c => new Point((int)c.Location.X, (int)c.Location.Y)).ToArray();

                if (center.IsWater)
                {
                    g.FillPolygon(center.Ocean ? Brushes.DodgerBlue : Brushes.CornflowerBlue, points);
                }
                else
                {
                    g.FillPolygon(center.Coast? Brushes.Wheat : Brushes.Green, points);
                }
            }

            foreach (var edge in map.Edges)
            {
                //g.DrawLine(Pens.White, (int)edge.DelunayEdge.A.X, (int)edge.DelunayEdge.A.Y, (int)edge.DelunayEdge.B.X, (int)edge.DelunayEdge.B.Y);

                if (Vector.IsInfinite(edge.VoronoiEdge.B))
                {
                    //g.DrawLine(Pens.Red, (int)edge.VoronoiEdge.A.X, (int)edge.VoronoiEdge.A.Y, 0, 0);
                }
                else
                {
                    g.DrawLine(Pens.Black, (int)edge.VoronoiEdge.A.X, (int)edge.VoronoiEdge.A.Y, (int)edge.VoronoiEdge.B.X, (int)edge.VoronoiEdge.B.Y);
                }
            }

            //foreach (var center in map.Centers.Skip(100).Take(1))
            //{
            //    foreach (var edge in center.Value.Borders)
            //    {
            //        g.DrawLine(Pens.Red, (float)edge.VoronoiEdge.A.X, (float)edge.VoronoiEdge.A.Y, (float)edge.VoronoiEdge.B.X, (float)edge.VoronoiEdge.B.Y);
            //    }

            //    var points = center.Value.Corners.Select(c => new PointF((float)c.Location.X, (float)c.Location.Y)).ToArray();

            //    g.FillPolygon(Brushes.Green, points);

            //    for (var i = 0; i < points.Length; i++)
            //    {
            //        g.DrawString(i.ToString(), SystemFonts.DefaultFont, Brushes.White, points[i].X, points[i].Y);

            //        g.DrawString(string.Format("x: {0} y: {1}", points[i].X, points[i].Y), SystemFonts.DefaultFont, Brushes.White, 100, 100 + 10 * i);
            //    }

            //    g.DrawEllipse(Pens.OrangeRed, (float)center.Key.X - 2, (float)center.Key.Y - 2, 4, 4);
            //}

            //foreach (var center in map.Centers.Values)
            //{
            //    var points = center.Corners.Select(c => new Point((int)c.Location.X, (int)c.Location.Y)).ToArray();

            //    g.FillPolygon(center.IsWater ? Brushes.CornflowerBlue : Brushes.Green, points);
            //}

            //foreach (var v in map.Corners)
            //    g.DrawEllipse(Pens.OrangeRed, (int)v.Value.Location.X - 2, (int)v.Value.Location.Y - 2, 4, 4);

            return bmp;
        }

        public static Bitmap GetVoronoiMap(int width, int height, VoronoiGraph graph)
        {
            var bmp = new Bitmap(width, height);
            var g = Graphics.FromImage(bmp);

            // draw a grid for visualisation
            //for (int w = 0; w <= width; w = w + 30)
            //    g.DrawLine(Pens.LightGray, w, 0, w, height);
            //for (int h = 0; h <= height; h = h + 30)
            //    g.DrawLine(Pens.LightGray, 0, h, width, h);

            // draw vertizes of voronoi
            foreach (var v in graph.Vertizes)
                g.DrawEllipse(Pens.OrangeRed, (int)v.X - 2, (int)v.Y - 2, 4, 4);


            // draw voronoi diagram
            foreach (var edge in graph.VoronoiEdges)
            {
                if (!edge.IsPartlyInfinite)
                {
                    // draw edges
                    g.DrawLine(Pens.Green, (int)edge.VoronoiA.X, (int)edge.VoronoiA.Y,
                      (int)edge.VoronoiB.X, (int)edge.VoronoiB.Y);
                }
            }

            return bmp;
        }

        /// <summary>
        /// Visualization of Delaunay Triangulation
        /// </summary>
        /// <param name="weight">Weight of result image.</param>
        /// <param name="height">Height of result image.</param>
        /// <param name="graph"></param>
        /// <param name="datapoints">Result bitmap.</param>
        /// <returns></returns>
        public static Bitmap GetDelaunayTriangulation(int width,
                      int height, VoronoiGraph graph, IEnumerable<Vector> datapoints)
        {
            var bmp = new Bitmap(width, height);
            var enumerable = datapoints as Vector[] ?? datapoints.ToArray();

            var g = Graphics.FromImage(bmp);

            foreach (var v in enumerable)
            {
                g.DrawRectangle(Pens.Yellow, (int)v.X - 2, (int)v.Y - 2, 4, 4);

                foreach (var edge in graph.VoronoiEdges)
                {
                    if ((Math.Abs(edge.DelunayB.X - v.X) < 1e-10) & (Math.Abs(edge.DelunayB.Y - v.Y) < 1e-10))
                    {
                        g.DrawLine(Pens.White, (int)edge.DelunayB.X, (int)edge.DelunayB.Y,
                                  (int)edge.DelunayA.X, (int)edge.DelunayA.Y);

                    }
                    else if ((Math.Abs(edge.DelunayA.X - v.X) < 1e-10) & (Math.Abs(edge.DelunayA.Y - v.Y) < 1e-10))
                    {
                        g.DrawLine(Pens.White, (int)edge.DelunayB.X, (int)edge.DelunayB.Y,
                                  (int)edge.DelunayA.X, (int)edge.DelunayA.Y);
                    }
                }

            }
            return bmp;
        }

        public static Bitmap DrawN(int n, int width, int height, VoronoiGraph graph)
        {
            var bmp = new Bitmap(width, height);
            var g = Graphics.FromImage(bmp);

            var edges = graph.VoronoiEdges;

            foreach (var v in graph.Vertizes)
                g.DrawEllipse(Pens.OrangeRed, (int)v.X - 4, (int)v.Y - 4, 8, 8);

            foreach (var edge in edges)
            {
                var pen1 = new Pen(Color.FromArgb(100, 255, 0, 255));
                var pen2 = new Pen(Color.FromArgb(100, 0, 255, 0));
                g.DrawRectangle(pen1, (int)edge.DelunayA.X - 2, (int)edge.DelunayA.Y - 2, 4, 4);
                g.DrawRectangle(pen2, (int)edge.DelunayB.X - 2, (int)edge.DelunayB.Y - 2, 4, 4);

                // g.DrawLine(Pens.White, (int)edge.LeftData.X, (int)edge.LeftData.Y,
                //                (int)edge.RightData.X, (int)edge.RightData.Y);

                if (!edge.IsPartlyInfinite)
                {
                    g.FillEllipse(Brushes.CornflowerBlue, (int)edge.VoronoiB.X - 3, (int)edge.VoronoiB.Y - 3, 6, 6);
                    g.DrawRectangle(Pens.Yellow, (int)edge.VoronoiA.X - 2, (int)edge.VoronoiA.Y - 2, 4, 4);

                    g.DrawLine(Pens.HotPink, (int)edge.VoronoiA.X, (int)edge.VoronoiA.Y,
                      (int)edge.VoronoiB.X, (int)edge.VoronoiB.Y);
                }
            }


            return bmp;
        }
    }
}
