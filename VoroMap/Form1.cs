﻿namespace VoroMap
{
    using System;
    using System.Collections.Generic;
    using System.Drawing;
    using System.Linq;
    using System.Windows.Forms;
    using FortuneVoronoi;
    using Base;
    using Voronoi;
    using Color = System.Drawing.Color;

    public partial class Form1 : Form
    {
        private Graphics Graphics { get; set; }

        public Form1()
        {
            InitializeComponent();
        }

        private static IEnumerable<Vector> Smooth(IEnumerable<Vector> vectors, int x, int y)
        {
            var previous = vectors.ToList();
            var next = new List<Vector>();

            for (var i = 0; i < 3; i++)
            {
                var map = Fortune.ComputeVoronoiGraph(previous);

                foreach (var vector in previous)
                {
                    var v0 = 0.0d;
                    var v1 = 0.0d;
                    var say = 0;

                    foreach (var edge in map.VoronoiEdges)
                    {
                        if (edge.DelunayB == vector || edge.DelunayA == vector)
                        {
                            var p0 = (edge.VoronoiA.X + edge.VoronoiB.X) / 2;
                            var p1 = (edge.VoronoiA.Y + edge.VoronoiB.Y) / 2;
                            v0 += double.IsNaN(p0) ? 0 : p0;
                            v1 += double.IsNaN(p1) ? 0 : p1;
                            say++;
                        }
                    }

                    var newX = vector.X;
                    var newY = vector.Y;

                    if (((v0 / say) < x) && ((v0 / say) > 0))
                    {
                        newX = v0 / say;
                    }

                    if (((v1 / say) < y) && ((v1 / say) > 0))
                    {
                        newY = v1 / say;
                    }

                    next.Add(new Vector(newX, newY));
                }

                previous = next;
                next = new List<Vector>();
            }

            return previous;
        }

        private void Draw()
        {
            this.Graphics = CreateGraphics();

            var height = ClientRectangle.Height - 50;
            var width = ClientRectangle.Width - 50;

            this.Graphics.Clip = new Region(ClientRectangle);

            Graphics.Clear(Color.Black);

            var vectors = new List<Vector>();
            var r = new Random();

            for (var i = 0; i < 600; i++)
            {
                var x = r.Next(50, width);
                var y = r.Next(50, height);
                vectors.Add(new Vector(x, y));
            }

            var datapoints = Smooth(vectors, width, height);
            var builder = new MapBuilder();

            var img = Maps.Draw(Width, Height, builder.BuildGraph(datapoints, 0, Width, 0, Height));

            //var graph = Fortune.ComputeVoronoiGraph(datapoints);


            // var img = Maps.DrawN(200, width, height, graph);

            //var voro = Maps.GetVoronoiMap(Width, Height, graph);

            //Graphics.DrawImage(voro, ClientRectangle.X, ClientRectangle.Y);

            //var delu = Maps.GetDelaunayTriangulation(Width, Height, graph, datapoints);
            //Graphics.DrawImage(delu, ClientRectangle.X, ClientRectangle.Y);
            Graphics.DrawImage(img, ClientRectangle.X, ClientRectangle.Y);
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            Draw();
        }
    }
}
